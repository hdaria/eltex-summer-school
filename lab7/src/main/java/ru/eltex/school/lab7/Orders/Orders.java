package ru.eltex.school.lab7.Orders;

import java.util.*;

public class Orders<T extends Order> {

    transient Map<Date, T> timeOrders;
    private List<T> orderList;

    public Orders() {
        orderList = new ArrayList<>();
        timeOrders = new TreeMap<>();
    }

    public boolean add(T order) {
        timeOrders.put(order.getCreateTime(), order);
        return orderList.add(order);
    }

    public boolean remove(T order) {
        return orderList.remove(order);
    }

    public boolean buy(ShoppingCart cart, Credentials user) {
        T ord = (T) new Order(cart, user);
        return orderList.add(ord);
    }

    public void checkWait() {
        Iterator<T> it = orderList.iterator();
        while (it.hasNext()) {
            T ord = it.next();
            if (ord.getStatus() == Status.WAIT) {
                ord.setStatus(Status.DONE);
            }
        }
    }

    public void checkDone() {
        Iterator<T> it = orderList.iterator();
        while (it.hasNext()) {
            T ord = it.next();
            if (ord.getStatus() == Status.DONE) {
                it.remove();
            }
        }
    }

    public void showOrders() {
        for (T ord : orderList) {
            ord.show();
        }
    }

    public Order searchByID(UUID ID) throws NullPointerException {
        Order order = null;
        Iterator<T> it = orderList.iterator();
        while (it.hasNext()) {
            T ord = it.next();
            if (ord.getId().equals(ID)) {
                order = ord;
            }
        }
        return order;
    }

    public boolean delById(UUID id) throws DeleteOrderException {
        boolean isDelete = false;
        Iterator<T> it = orderList.iterator();
        while (it.hasNext()) {
            T ord = it.next();
            if (ord.getId().equals(id)) {
                it.remove();
                isDelete = true;
                break;
            }
        }
        if (!isDelete) throw new DeleteOrderException("Заказ отсутствует", 1);

        return isDelete;
    }


    public List<T> getOrder() {
        return this.orderList;
    }

}