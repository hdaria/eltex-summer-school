package ru.eltex.school.lab7;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.eltex.school.lab7.AChecks.GoodsGenerator;
import ru.eltex.school.lab7.Goods.Goods;
import ru.eltex.school.lab7.ManageOrders.AManageOrder;
import ru.eltex.school.lab7.ManageOrders.ManagerOrderJSON;
import ru.eltex.school.lab7.Orders.DeleteOrderException;
import ru.eltex.school.lab7.Orders.Orders;

import java.io.File;
import java.io.IOException;
import java.util.UUID;


@RestController
public class MyController {

    public final static String ORDER_JSON = "orders.json";
    private final static Logger logger = Logger.getLogger(MyController.class);
    private Orders orders = null;

    @RequestMapping(value = "/", method = RequestMethod.GET, params = "command",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Object allRequests(@RequestParam("command") String command,
                              @RequestParam(value = "order_id", required = false) String orderId,
                              @RequestParam(value = "card_id", required = false) String cardId) {
        AManageOrder manageOrder = new ManagerOrderJSON();

        try {
            orders = loadOrders();
        } catch (DeleteOrderException e) {
            logger.error(e.getMessage());
            return e.getErrorCode();
        }

        if (command.equals("readAll")) { // вывести все заказы
            return orders.getOrder();

        } else if ((command.equals("readById")) && (orderId != null)) { // вывести заказ с определенным id
            UUID uuid = UUID.fromString(orderId);
            return orders.searchByID(uuid);

        } else if ((command.equals("delById")) && (orderId != null)) { // удалить заказ по идентефикатору
            int code = 0;
            try {
                UUID uuid = UUID.fromString(orderId);
                orders.delById(uuid);
                manageOrder.saveAll(orders);
            } catch (IllegalArgumentException e) {
                logger.error("Неправильный формат id", e);


            } catch (DeleteOrderException er) {
                logger.error(er.getMessage());
                code = er.getErrorCode();
            } catch (IOException e) {
                logger.error("Ошибка сохранения заказов", e);
            }

            return code;

        } else if ((command.equals("addToCard")) && (cardId != null)) { // добавить товар в корзину (по id заказа)
            UUID uuid = UUID.fromString(cardId);
            Goods product = new GoodsGenerator().generateGood();
            orders.searchByID(uuid).addToCard(product);
            try {
                manageOrder.saveAll(orders);
            } catch (IOException e) {
                logger.error("Ошибка сохранения заказов", e);
            }
            return product.getID();

        } else {
            return "Wrong command";
        }
    }

    public Orders loadOrders() throws DeleteOrderException {
        File file = new File(ORDER_JSON);
        AManageOrder manageOrder = new ManagerOrderJSON();
        Orders ords = null;
        if (file.exists() && (file.length() != 0)) { // считывание заказов из файла
            try {
                ords = manageOrder.readAll();
            } catch (IOException e) {
                logger.error("Ошибка считывания заказов", e);
            }
        } else if (ords == null) {
            throw new DeleteOrderException("Файл отсутствует или поврежден", 2);
        }
        return ords;
    }


}
