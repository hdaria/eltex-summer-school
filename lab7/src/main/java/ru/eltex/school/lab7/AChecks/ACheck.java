package ru.eltex.school.lab7.AChecks;

import ru.eltex.school.lab7.Orders.Order;
import ru.eltex.school.lab7.Orders.Orders;

public abstract class ACheck implements Runnable {

    public boolean frun = true;
    protected Orders<Order> orders;

    public synchronized void setOrders(Orders<Order> orders) {
        this.orders = orders;
    }

}
