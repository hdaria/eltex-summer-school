package ru.eltex.school.lab7.ClientServer;

import ru.eltex.school.lab7.Orders.Order;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class ClientIpMap {

    private Map<UUID, ClientInfo> ipMap;

    public ClientIpMap() {
        ipMap = new ConcurrentHashMap<>();
    }

    public void add(Order ord, int port, String ip) {
        ipMap.put(ord.getId(), new ClientInfo(port, ip));
    }

    public String getIp(UUID id) {
        return ipMap.get(id).getIp();
    }

    public int getPort(UUID id) {
        return ipMap.get(id).getPort();
    }

}

class ClientInfo {
    private int port;
    private String ip;

    ClientInfo(int port, String ip) {
        this.port = port;
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public String getIp() {
        return ip;
    }
}