package ru.eltex.school.lab7.Goods;

public interface ICrudAction {

    void read();

    void update();

    void delete();
}
