package ru.eltex.school.lab7.ManageOrders;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.eltex.school.lab7.Orders.Order;
import ru.eltex.school.lab7.Orders.Orders;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class ManagerOrderJSON extends AManageOrder {

    public static final String JSONFILE = "orders.json";

    @Override
    public Order readById(UUID id) {
        Order newOrd = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            FileInputStream fis = new FileInputStream(JSONFILE);
            List<Order> orderList = mapper.readValue(fis, new TypeReference<List<Order>>() {
            });
            newOrd = orderList.stream().filter(s -> s.getId().equals(id)).findFirst().orElse(null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newOrd;
    }

    @Override
    public void saveById(Orders orders, UUID id) throws IOException {
        super.saveById(orders, id);
        if (readById(id) == null) {
            ObjectMapper mapper = new ObjectMapper();
            FileInputStream fis = new FileInputStream(JSONFILE);
            List<Order> orderList;
            orderList = mapper.readValue(fis, new TypeReference<List<Order>>() {
            });
            orderList.add(order);
            try (FileOutputStream fos = new FileOutputStream(JSONFILE)) {
                mapper.writeValue(fos, orderList);
            }
        }
    }

    @Override
    public Orders readAll() {
        Orders newOrders = new Orders();
        List<Order> orderList;
        try {
            FileInputStream fis = new FileInputStream(JSONFILE);
            ObjectMapper mapper = new ObjectMapper();
            orderList = mapper.readValue(fis, new TypeReference<List<Order>>() {
            });
            Iterator<Order> it = orderList.iterator();
            while (it.hasNext()) {
                Order order = it.next();
                newOrders.add(order);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return newOrders;
    }

    @Override
    public void saveAll(Orders orders) throws IOException {
        ords = orders;
        try (FileOutputStream fos = new FileOutputStream(JSONFILE)) {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(fos, ords.getOrder());
        }
    }
}
