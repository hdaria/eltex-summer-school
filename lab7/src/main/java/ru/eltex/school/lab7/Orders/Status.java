package ru.eltex.school.lab7.Orders;

import java.io.Serializable;

public enum Status implements Serializable {WAIT, DONE}
