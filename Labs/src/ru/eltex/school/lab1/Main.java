package ru.eltex.school.lab1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int count;
        String type;

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите количество товаров: ");
        if (sc.hasNextInt()) {
            count = sc.nextInt();
            Goods items[] = new Goods[count];

            System.out.println("Введите тип товара (Mobile, Smart, Tablet):");

            type = sc.next();

            if (type.toLowerCase().equals("mobile")) {
                for (int i = 0; i < count; i++) {
                    items[i] = new Mobile();
                    // items[i].create();
                    items[i].update();
                    items[i].read();
                }
            } else if (type.equals("smart")) {
                for (int i = 0; i < count; i++) {
                    items[i] = new Smart();
                    // items[i].create();
                    items[i].update();
                    items[i].read();

                }
            } else if (type.equals("tablet")) {
                for (int i = 0; i < count; i++) {
                    items[i] = new Tablet();
                    items[i].update();
                    items[i].read();
                }
            } else {
                System.out.println("Error");
            }


            for (int i = 0; i < count; i++)
                items[i].delete();


        } else {
            System.out.println("Ошибка");
        }
        sc.close();
    }
}
