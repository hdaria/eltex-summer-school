package ru.eltex.school.lab3;

import ru.eltex.school.lab3.Goods.Goods;

import java.util.*;

public class ShoppingCart<T extends Goods> {

    List<T> cart;
    Set<UUID> ID;

    public ShoppingCart() {
        this.cart = new LinkedList<>();
        this.ID = new HashSet<>();
    }

    public boolean add(T product) {
        ID.add(product.getID());
        return cart.add(product);
    }

    public boolean delete(T product) {
        ID.remove(product.getID());
        return cart.remove(product);
    }

    public boolean searchCart(UUID id) {
        return ID.contains(id);
    }

    public void showCart() {
        for (T prod : cart) {
            prod.read();
        }
    }

}
