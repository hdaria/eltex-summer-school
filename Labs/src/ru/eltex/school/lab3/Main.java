package ru.eltex.school.lab3;


import ru.eltex.school.lab3.Goods.Goods;
import ru.eltex.school.lab3.Goods.Mobile;
import ru.eltex.school.lab3.Goods.Smart;
import ru.eltex.school.lab3.Goods.Tablet;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        int count = 1;
        String type = "Mobile";

        Goods items[] = new Goods[count];

        if (type.toLowerCase().equals("mobile")) {
            for (int i = 0; i < count; i++) {
                items[i] = new Mobile();
            }
        } else if (type.equals("smart")) {
            for (int i = 0; i < count; i++) {
                items[i] = new Smart();
            }
        } else if (type.equals("tablet")) {
            for (int i = 0; i < count; i++) {
                items[i] = new Tablet();
            }
        } else {
            System.out.println("Error");
        }

        ShoppingCart cart = new ShoppingCart();

        for (Goods prod : items) {
            cart.add(prod);
        }
        cart.showCart();
        System.out.println("_____");
        cart.delete(items[0]);
        cart.showCart();
        System.out.println("_____");
        boolean b = cart.searchCart(items[0].getID());
        System.out.println("Товар в корзине: " + b);

        Credentials user = new Credentials();

        Order ord = new Order(cart, user);
        Orders orders = new Orders();
        orders.add(ord);
        orders.showOrders();
        System.out.println("_____");

        TimeUnit.SECONDS.sleep(5);
        orders.check();
        orders.showOrders();
        System.out.println("__check__");

        cart = new ShoppingCart();

        cart.add(items[0]);
        cart.showCart();
        System.out.println("_____");
        orders.buy(cart, user);
        orders.showOrders();
        System.out.println("_____");
        orders.check();
        orders.showOrders();
        System.out.println("_____");


    }
}
