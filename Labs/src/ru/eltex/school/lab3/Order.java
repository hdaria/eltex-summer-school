package ru.eltex.school.lab3;

import java.util.Date;
import java.util.Random;

public class Order {

    private ShoppingCart cart;
    private Credentials user;
    private Date createTime;
    private int waitTime;
    private Status stat;

    Random random = new Random();


    Order(ShoppingCart cart1, Credentials user1) {
        cart = cart1;
        user = user1;
        createTime = new Date(System.currentTimeMillis());
        waitTime = 2000 + random.nextInt(8001);
        stat = Status.WAIT;
    }

    public void show() {
        System.out.println("Состав заказа:");
        this.cart.showCart();
        System.out.println("Данные заказчика: ");
        this.user.show();
        System.out.println("Статус заказа: " + this.stat);
        System.out.println("Время обработки заказа: " + this.waitTime / 1000d);
        System.out.println(" ");
    }

    public int getWaitTime() {
        return this.waitTime;
    }

    public Date getTime() {
        return this.createTime;
    }

    public long getCreateTime() {
        return this.createTime.getTime();
    }

    public Status getStatus() {
        return this.stat;
    }
}
