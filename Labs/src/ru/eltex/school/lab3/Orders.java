package ru.eltex.school.lab3;

import java.util.*;

public class Orders<T extends Order> {

    List<T> orderList;
    Map<Date, T> timeOrders;

    public Orders() {
        orderList = new ArrayList<>();
        timeOrders = new TreeMap<>();
    }

    public boolean add(T order) {
        timeOrders.put(order.getTime(), order);
        return orderList.add(order);
    }

    public boolean buy(ShoppingCart cart, Credentials user) {
        T ord = (T) new Order(cart, user);
        return orderList.add(ord);
    }

    public void check() {
        Iterator<T> it = orderList.iterator();
        while (it.hasNext()) {
            T ord = it.next();
            if ((System.currentTimeMillis() > (ord.getWaitTime() + ord.getCreateTime())) || ord.getStatus() == Status.DONE) {
                it.remove();
            }

        }
    }

    public void showOrders() {
        for (T ord : orderList) {
            ord.show();
        }
    }

}