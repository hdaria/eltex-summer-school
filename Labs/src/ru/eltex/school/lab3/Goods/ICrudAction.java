package ru.eltex.school.lab3.Goods;

public interface ICrudAction {

    void read();

    void update();

    void delete();
}
