package ru.eltex.school.lab3;

import java.util.Scanner;
import java.util.UUID;

public class Credentials {

    Scanner scan = new Scanner(System.in);

    UUID id;
    private String name;
    private String surname;
    private String patronymic;
    private String email;

    Credentials() {
        id = UUID.randomUUID();
        name = "Иван";
        surname = "Иванов";
        patronymic = "Иванович";
        email = "ivan@mailbox.com";
    }

    public void update() {
        System.out.println("Введите фамилию:");
        this.surname = scan.next();
        System.out.println("Введите имя:");
        this.name = scan.next();
        System.out.println("Введите отчество:");
        this.patronymic = scan.next();
        System.out.println("Введите e-mail:");
        this.email = scan.next();
    }


    public void show() {
        System.out.println("ID: " + this.id);
        System.out.println("Фамилия: " + this.surname);
        System.out.println("Имя: " + this.name);
        System.out.println("Отчество: " + this.patronymic);
        System.out.println("E-mail: " + this.email);
        System.out.println(" ");
    }

    public void delete() {
        this.name = null;
        this.surname = null;
        this.patronymic = null;
        this.email = null;
    }
}
