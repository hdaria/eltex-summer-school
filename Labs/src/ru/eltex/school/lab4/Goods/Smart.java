package ru.eltex.school.lab4.Goods;

import java.util.Random;
import java.util.Scanner;

public class Smart extends Goods {

    Random random = new Random();
    Scanner scan = new Scanner(System.in);

    private String simTypes[] = {"SIM", "microSIM", "nanoSIM"};
    private int simType;
    private int simNumber;

    public Smart() {
        super();
        simType = random.nextInt(simTypes.length);
        simNumber = 1 + random.nextInt(2);
    }


    @Override
    public void read() {
        super.read();
        System.out.println("Тип сим-карты: " + simTypes[this.simType]);
        System.out.println("Количество сим-карт: " + this.simNumber);
        System.out.println(" ");
    }

    @Override
    public void update() {
        super.update();
        System.out.println("Введите тип сим-карты (0 - SIM, 1 - microSIM, 2 - nanoSIM):");
        this.simType = scan.nextInt();
        System.out.println("Введите количество сим-карт:");
        this.simNumber = scan.nextInt();
    }

    @Override
    public void delete() {
        super.delete();
        this.simNumber = 0;
        this.simType = -1;
    }
}
