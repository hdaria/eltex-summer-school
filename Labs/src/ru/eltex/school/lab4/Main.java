package ru.eltex.school.lab4;


import ru.eltex.school.lab4.AChecks.ACheck;
import ru.eltex.school.lab4.AChecks.ACheckDone;
import ru.eltex.school.lab4.AChecks.ACheckWait;
import ru.eltex.school.lab4.AChecks.OrderGenerator;
import ru.eltex.school.lab4.Orders.Orders;


public class Main {

    public static void main(String[] args) {

        Orders orders = new Orders();

        OrderGenerator gen = new OrderGenerator();
        gen.setOrders(orders);
        Thread genT = new Thread(gen);
        genT.start();

        ACheck waitingOrders = new ACheckWait();
        waitingOrders.setOrders(orders);
        Thread waitT = new Thread(waitingOrders);
        waitT.start();

        ACheck doneOrders = new ACheckDone();
        doneOrders.setOrders(orders);
        Thread doneT = new Thread(doneOrders);
        doneT.start();


        OrderGenerator gen1 = new OrderGenerator();
        gen1.setOrders(orders);
        Thread genT1 = new Thread(gen1);
        genT1.start();

        try {
            genT.join();
            genT1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }



        doneOrders.frun = false;
        waitingOrders.frun = false;

        try {
            waitT.join();
            doneT.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
