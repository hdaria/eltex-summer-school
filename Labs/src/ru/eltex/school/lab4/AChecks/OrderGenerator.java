package ru.eltex.school.lab4.AChecks;

import ru.eltex.school.lab4.Goods.Goods;
import ru.eltex.school.lab4.Goods.Mobile;
import ru.eltex.school.lab4.Goods.Smart;
import ru.eltex.school.lab4.Goods.Tablet;
import ru.eltex.school.lab4.Orders.Credentials;
import ru.eltex.school.lab4.Orders.Order;
import ru.eltex.school.lab4.Orders.Orders;
import ru.eltex.school.lab4.Orders.ShoppingCart;

public class OrderGenerator implements Runnable {

    private Orders<Order> orders;

    public synchronized void setOrders(Orders<Order> orders) {
        this.orders = orders;
    }

    @Override
    public void run() {
        try {
            int countOrd = 1 + (int) (Math.random() * 5);

            for (int i = 0; i < countOrd; i++) {
                Credentials user = new Credentials();
                ShoppingCart<Goods> cart = new ShoppingCart<>();
                int countGoods = 1 + (int) (Math.random() * 5);

                for (int j = 0; j < countGoods; j++) {
                    Goods product;
                    int choice = (int) (Math.random() * 3);
                    switch (choice) {
                        case 0:
                            product = new Mobile();
                            break;
                        case 1:
                            product = new Tablet();
                            break;
                        case 2:
                            product = new Smart();
                            break;
                        default:
                            product = new Mobile();
                    }
                    cart.add(product);
                }

                orders.buy(cart, user);
            }
            Thread.sleep(1000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
