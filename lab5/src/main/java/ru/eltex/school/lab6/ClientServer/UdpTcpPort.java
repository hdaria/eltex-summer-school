package ru.eltex.school.lab6.ClientServer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpTcpPort implements Runnable {

    public static final int[] UDPPORT = {6660, 6661, 6662};
    private String message;
    private String host;

    public UdpTcpPort(String mes, String address) {
        message = mes;
        host = address;
    }

    @Override
    public void run() {
        while (true) {
            try {
                byte[] data = message.getBytes();
                InetAddress addr = InetAddress.getByName(host);
                DatagramSocket ds = new DatagramSocket();
                ds.setBroadcast(true);
                for (int port : UDPPORT) {
                    DatagramPacket pack = new DatagramPacket(data, data.length, addr, port);
                    ds.send(pack);
                }
                ds.close();
                Thread.sleep(1000);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }


}
