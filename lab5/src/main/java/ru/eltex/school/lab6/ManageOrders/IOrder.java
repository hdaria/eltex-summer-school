package ru.eltex.school.lab6.ManageOrders;

import ru.eltex.school.lab6.Orders.Order;
import ru.eltex.school.lab6.Orders.Orders;

import java.io.IOException;
import java.util.UUID;

public interface IOrder {

    Order readById(UUID id) throws IOException;

    void saveById(Orders orders, UUID id) throws IOException;

    Orders readAll() throws IOException;

    void saveAll(Orders orders) throws IOException;
}
