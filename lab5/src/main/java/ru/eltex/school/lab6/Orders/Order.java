package ru.eltex.school.lab6.Orders;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class Order implements Serializable {

    transient Random random = new Random();
    @JsonProperty("cart")
    private ShoppingCart cart;
    @JsonProperty("user")
    private Credentials user;
    private Date createTime;
    private int waitTime;
    private Status stat;
    private UUID id;

    @JsonCreator
    public Order(@JsonProperty("cart") ShoppingCart cart1, @JsonProperty("user") Credentials user1) {
        id = UUID.randomUUID();
        cart = cart1;
        user = user1;
        createTime = new Date(System.currentTimeMillis());
        waitTime = 2000 + random.nextInt(8001);
        stat = Status.WAIT;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public Credentials getUser() {
        return user;
    }

    public Status getStat() {
        return stat;
    }

    public void show() {
        System.out.println("Номер заказа: " + this.id);
        System.out.println("Состав заказа:");
        this.cart.showCart();
        System.out.println("Данные заказчика: ");
        this.user.show();
        System.out.println("Статус заказа: " + this.stat);
       // System.out.println("Время обработки заказа: " + this.waitTime / 1000d);
        System.out.println(" ");
    }

    public int getWaitTime() {
        return this.waitTime;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public Status getStatus() {
        return this.stat;
    }

    public void setStatus(Status status) {
        this.stat = status;
    }

    public UUID getId() {
        return this.id;
    }
}
