package ru.eltex.school.lab6.AChecks;

import ru.eltex.school.lab5.Orders.Order;
import ru.eltex.school.lab5.Orders.Orders;

public abstract class ACheck implements Runnable {

    public boolean frun = true;
    protected Orders<Order> orders;

    public synchronized void setOrders(Orders<Order> orders) {
        this.orders = orders;
    }

}
