package ru.eltex.school.lab6.Goods;

import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;

public class Tablet extends Goods implements Serializable {

    transient Random random = new Random();
    transient Scanner scan = new Scanner(System.in);

    private String VideoProc[] = {"Qualcomm Adreno", "NVIDIA GeForce Tegra", "PowerVR", "ARM Mail"};
    private int video;
    private String ScreenResol[] = {"480x320", "800x480", "960x640", "1280x800"};
    private int screen;

    public Tablet() {
        super();
        video = random.nextInt(VideoProc.length);
        screen = random.nextInt(ScreenResol.length);
    }

    public int getVideo() {
        return video;
    }

    public int getScreen() {
        return screen;
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Процессор: " + VideoProc[this.video]);
        System.out.println("Разешение экрана: " + ScreenResol[this.screen]);
        System.out.println(" ");
    }

    @Override
    public void update() {
        super.update();
        System.out.println("Введите тип видеопроцессора [0-" + (VideoProc.length - 1) + "]:");
        this.video = scan.nextInt();
        System.out.println("Вседите разрешение экрана [0-" + (ScreenResol.length - 1) + "]:");
        this.screen = scan.nextInt();
    }

    @Override
    public void delete() {
        super.delete();
    }
}
