package ru.eltex.school.lab6.Orders;

import java.io.Serializable;

public enum Status implements Serializable {WAIT, DONE}
