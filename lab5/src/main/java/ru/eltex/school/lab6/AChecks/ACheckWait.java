package ru.eltex.school.lab6.AChecks;

public class ACheckWait extends ACheck {

    @Override
    public void run() {

        try {
            while (frun == true) {
                orders.checkWait();
                Thread.sleep(1000);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
