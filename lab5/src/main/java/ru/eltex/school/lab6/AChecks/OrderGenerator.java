package ru.eltex.school.lab6.AChecks;

import ru.eltex.school.lab6.Goods.Goods;
import ru.eltex.school.lab6.Goods.Mobile;
import ru.eltex.school.lab6.Goods.Smart;
import ru.eltex.school.lab6.Goods.Tablet;
import ru.eltex.school.lab6.Orders.Credentials;
import ru.eltex.school.lab6.Orders.Order;
import ru.eltex.school.lab6.Orders.Orders;
import ru.eltex.school.lab6.Orders.ShoppingCart;

public class OrderGenerator implements Runnable {

    public static final int MAX_GOODS = 5;
    public static final int MAX_ORDERS = 5;
    private Orders<Order> orders;

    public OrderGenerator(Orders ord) {
        orders = ord;

    }

    @Override
    public void run() {
        try {
            //int countOrd = 2 + (int) (Math.random() * MAX_ORDERS);
            int countOrd = 1;
            for (int i = 0; i < countOrd; i++) {
                Credentials user = new Credentials();
                ShoppingCart<Goods> cart = new ShoppingCart<>();
                int countGoods = 1 + (int) (Math.random() * MAX_GOODS);

                for (int j = 0; j < countGoods; j++) {
                    Goods product;
                    int choice = (int) (Math.random() * 3);
                    switch (choice) {
                        case 0:
                            product = new Mobile();
                            break;
                        case 1:
                            product = new Tablet();
                            break;
                        case 2:
                            product = new Smart();
                            break;
                        default:
                            product = new Mobile();
                    }
                    cart.add(product);
                }

                orders.buy(cart, user);
            }
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
