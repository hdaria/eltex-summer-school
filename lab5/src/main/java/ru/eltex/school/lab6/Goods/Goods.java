package ru.eltex.school.lab6.Goods;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")

public abstract class Goods implements ICrudAction, Serializable {

    public static final int MAX_PRICE = 1000;
    static private int goodsCount;
    transient Scanner scan = new Scanner(System.in);
    transient Random random = new Random();
    private UUID id;
    private int name;
    private String Names[] = {"Мобильный телефон", "Смартфон", "Планшет"};
    private int firm;
    private String Firms[] = {"Sony", "Samsung", "Panasonic", "Apple", "Xiaomi", "Siemens", "Toshiba", "Huawei"};
    private int model;
    private double price;
    private String OS[] = {"None", "Android", "iOS", "Tizen", "WindowsMobile", "BlackBerry OS", "Sailfish OS", "Fire OS"};
    private int os;

    Goods() {
        id = UUID.randomUUID();
        name = random.nextInt(Names.length);
        firm = random.nextInt(Firms.length);
        model = 1000 + random.nextInt(9000);
        price = Math.random() * MAX_PRICE;
        os = random.nextInt(OS.length);
        goodsCount++;

    }

    public int getName() {
        return name;
    }

    public int getFirm() {
        return firm;
    }

    public int getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }

    public int getOs() {
        return os;
    }

    @Override
    public void read() {
        System.out.println("Название товара: " + Names[this.name]);
        System.out.println("Производитель товара: " + Firms[this.firm]);
        System.out.println("Модель товара: " + this.model);
        System.out.println("Цена товара: " + this.price);
        System.out.println("Операционная система товара: " + OS[this.os]);
    }

    @Override
    public void update() {
        System.out.println("Введите название товара (0 - Мобильнй телефон, 1 - Смартфон, 2 - Планшет):");
        this.name = scan.nextInt();
        System.out.println("Введите производителя [0-" + (Firms.length - 1) + "]: ");
        this.firm = scan.nextInt();
        System.out.println("Введите модель:");
        this.model = scan.nextInt();
        System.out.println("Введите цену:");
        this.price = scan.nextDouble();
        System.out.println("Введите тип операционной системы [0-" + (OS.length - 1) + "]:");
        this.os = scan.nextInt();

    }

    @Override
    public void delete() {

        this.name = -1;
        this.firm = -1;
        this.model = 0;
        this.os = -1;
        this.price = 0;
        goodsCount--;
    }

    public UUID getID() {
        return this.id;
    }
}
