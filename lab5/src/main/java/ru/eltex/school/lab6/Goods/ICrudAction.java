package ru.eltex.school.lab6.Goods;

public interface ICrudAction {

    void read();

    void update();

    void delete();
}
