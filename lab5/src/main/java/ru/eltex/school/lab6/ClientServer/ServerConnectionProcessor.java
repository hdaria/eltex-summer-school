package ru.eltex.school.lab6.ClientServer;

import ru.eltex.school.lab6.Orders.Order;
import ru.eltex.school.lab6.Orders.Orders;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class ServerConnectionProcessor extends Thread {
    private Socket socket;
    private Orders orders;
    private ClientIpMap map;
    private int udpPort;

    ServerConnectionProcessor(Socket s) {
        socket = s;
    }

    public void setParameters(Orders ords, ClientIpMap ipMap) {
        orders = ords;
        map = ipMap;
    }

    @Override
    public void run() {
        try {
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            Order order = (Order) ois.readObject();
            udpPort = (int) ois.readObject();

            synchronized (orders) {
                orders.add(order);
            }

            String ipaddress = socket.getInetAddress().toString();
            ipaddress = ipaddress.substring(1);
            map.add(order, udpPort, ipaddress);

            ois.close();
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
