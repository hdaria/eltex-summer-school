package ru.eltex.school.lab6.Goods;

import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;

public class Mobile extends Goods implements Serializable {

    transient Scanner scan = new Scanner(System.in);
    transient Random random = new Random();
    private String casesType[] = {"Classic", "Clamshell"};
    private int typeOfCase;

    public Mobile() {
        super();
        typeOfCase = random.nextInt(casesType.length);
    }

    public int getTypeOfCase() {
        return typeOfCase;
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Тип корпуса телефона: " + casesType[this.typeOfCase]);
        System.out.println(" ");
    }

    @Override
    public void update() {
        super.update();
        System.out.println("Введите тип корпуса телефона (0 - Classic, 1 - Clamshell):");
        this.typeOfCase = scan.nextInt();
    }

    @Override
    public void delete() {
        super.delete();
        this.typeOfCase = -1;
    }
}
