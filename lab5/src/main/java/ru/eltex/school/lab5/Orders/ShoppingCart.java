package ru.eltex.school.lab5.Orders;

import ru.eltex.school.lab5.Goods.Goods;

import java.io.Serializable;
import java.util.*;

public class ShoppingCart<T extends Goods> implements Serializable {

    List<T> cart;
    Set<UUID> ID;

    public ShoppingCart() {
        this.cart = new LinkedList<>();
        this.ID = new HashSet<>();
    }

    public List<T> getCart() {
        return cart;
    }

    public Set<UUID> getID() {
        return ID;
    }

    public boolean add(T product) {
        ID.add(product.getID());
        return cart.add(product);
    }

    public boolean delete(T product) {
        ID.remove(product.getID());
        return cart.remove(product);
    }

    public boolean searchCart(UUID id) {
        return ID.contains(id);
    }

    public void showCart() {
        for (T prod : cart) {
            prod.read();
        }
    }

}
