package ru.eltex.school.lab5.ManageOrders;

import ru.eltex.school.lab5.Orders.Order;
import ru.eltex.school.lab5.Orders.Orders;

import java.io.IOException;
import java.util.UUID;

public abstract class AManageOrder implements IOrder {

    protected Orders ords;
    protected Order order;

    @Override
    public Order readById(UUID id) throws IOException {
        return null;
    }

    @Override
    public void saveById(Orders orders, UUID id) throws IOException {
        order = orders.searchByID(id);
    }

    @Override
    public Orders readAll() throws IOException {
        return null;
    }

    @Override
    public void saveAll(Orders orders) throws IOException {

    }
}
