package ru.eltex.school.lab5;


import ru.eltex.school.lab5.AChecks.OrderGenerator;
import ru.eltex.school.lab5.ManageOrders.AManageOrder;
import ru.eltex.school.lab5.ManageOrders.ManagerOrderJSON;
import ru.eltex.school.lab5.Orders.Order;
import ru.eltex.school.lab5.Orders.Orders;

import java.io.IOException;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {

        Orders orders = new Orders();
        Orders newOrders = new Orders();
        Order newOrder = null;

        OrderGenerator gen = new OrderGenerator(orders);
        Thread genT = new Thread(gen);
        genT.start();

        try {
            genT.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Order order = (Order) orders.getOrder().get(0);
        UUID id = order.getId();
        Order order1 = (Order) orders.getOrder().get(1);
        UUID id1 = order.getId();


        AManageOrder ordToJson = new ManagerOrderJSON();
        try {
            //ordToJson.saveAll(orders);
            //ordToBin.saveById(orders, id);
            ordToJson.saveById(orders, id);
            ordToJson.saveById(orders, id1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            //newOrders = ordToJson.readAll();
            newOrder = ordToJson.readById(id);
        } catch (IOException e) {
            e.printStackTrace();
        }

        newOrders.add(newOrder);

        orders.showOrders();
        System.out.println("___");

        newOrders.showOrders();

    }
}
