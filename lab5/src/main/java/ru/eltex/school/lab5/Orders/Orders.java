package ru.eltex.school.lab5.Orders;

import java.util.*;

public class Orders<T extends Order> {

    transient Map<Date, T> timeOrders;
    private List<T> orderList;

    public Orders() {
        orderList = new ArrayList<>();
        timeOrders = new TreeMap<>();
    }

    public boolean add(T order) {
        timeOrders.put(order.getCreateTime(), order);
        return orderList.add(order);
    }

    public boolean buy(ShoppingCart cart, Credentials user) {
        T ord = (T) new Order(cart, user);
        return orderList.add(ord);
    }

    public void checkWait() {
        Iterator<T> it = orderList.iterator();
        while (it.hasNext()) {
            T ord = it.next();
            if (ord.getStatus() == Status.WAIT) {
                ord.setStatus(Status.DONE);
            }

        }
    }

    public void checkDone() {
        Iterator<T> it = orderList.iterator();
        while (it.hasNext()) {
            T ord = it.next();
            if (ord.getStatus() == Status.DONE) {
                it.remove();
            }

        }
    }

    public void showOrders() {
        for (T ord : orderList) {
            ord.show();
        }
    }

    public Order searchByID(UUID ID) throws NullPointerException {
        Order order = null;
        Iterator<T> it = orderList.iterator();
        while (it.hasNext()) {
            T ord = it.next();
            if (ord.getId() == ID) {
                order = ord;
            }
        }
        return order;
    }

    public List<T> getOrder() {
        return this.orderList;
    }

}