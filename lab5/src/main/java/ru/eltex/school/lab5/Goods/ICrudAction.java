package ru.eltex.school.lab5.Goods;

public interface ICrudAction {

    void read();

    void update();

    void delete();
}
