package ru.eltex.school.lab5.Orders;

import java.io.Serializable;

public enum Status implements Serializable {WAIT, DONE}
