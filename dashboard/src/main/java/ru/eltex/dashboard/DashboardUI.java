package ru.eltex.dashboard;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.WebBrowser;
import com.vaadin.ui.*;

import javax.servlet.annotation.WebServlet;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Khodorchenko Daria
 *
 *Главный класс, отвечающий за пользовательский интерфейс
 *
 */

@Theme("valo")
public class DashboardUI extends UI {
	
    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();

        HorizontalLayout widgets = new HorizontalLayout();
        widgets.setWidth("600px");
        widgets.setHeight("265px");

        ValuteWidget valute = new ValuteWidget();//курс валют
        widgets.addComponent(valute);
        widgets.setExpandRatio(valute, 1.0f);
        valute.setSizeFull();

        WeatherWidget weather = new WeatherWidget();//погода
        widgets.addComponent(weather);
        widgets.setExpandRatio(weather, 1.0f);
        weather.setSizeFull();

        VisitWidget visits = new VisitWidget();//счетчик посещений
        widgets.addComponent(visits);
        widgets.setExpandRatio(visits, 1.0f);
        visits.setSizeFull();

        HorizontalLayout info = new HorizontalLayout();

        Date dateNow = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("E dd.MM.yyyy ', ' HH:mm:ss");
        Label dateToday = new Label("Информация актуальна на: " + formatForDateNow.format(dateNow));
        info.addComponent(dateToday);
        info.setComponentAlignment(dateToday, Alignment.MIDDLE_LEFT);

        WebBrowser browser = Page.getCurrent().getWebBrowser();
        String ip = browser.getAddress();
        Label ipAddress = new Label("Ваш IP-адрес: " + ip);
        info.addComponent(ipAddress);
        info.setComponentAlignment(ipAddress, Alignment.MIDDLE_RIGHT);

        layout.addComponents(widgets, info);
        setContent(layout);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = DashboardUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
