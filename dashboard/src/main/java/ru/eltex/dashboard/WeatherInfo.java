package ru.eltex.dashboard;

import com.jayway.jsonpath.JsonPath;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
/**
 * Класс, отвечающий за получение погодных данных
 * @author Khodorchenko Daria
 *
 */
public class WeatherInfo extends Info {

    private final static String WEATHER_APP_ID = "&APPID=48edef8f62799053ac819d51dc082f86&units=metric";
    private final static String WEATHER_APP_QUERY = "https://api.openweathermap.org/data/2.5/weather?id=";
    private final static String WEATHER_TOMMOROW_APP_QUERY = "https://api.openweathermap.org/data/2.5/forecast?id=";

    //список городов с кодами вынесен в файл конфигурации
    private final static String CITIES_LIST = "src/main/resources/cities.properties";

    private String tempToday;
    private String tempTommorow;
    private String city;

    private String textToday;
    private String textTommorow;

    private FileInputStream fis;
    private Properties prop = new Properties();

    /**
     * Получает погоду сейчас и на завтра по названию города
     * @param city Строка-название города
     * @throws IOException Может возникнуть при получении списка городов из файла конфигурации
     */
    public WeatherInfo(String city) throws IOException {
        fis = new FileInputStream(CITIES_LIST);
        prop.load(fis);

        setCity(city);
        textToday = jsonFromUrl(new URL(WEATHER_APP_QUERY + this.city + WEATHER_APP_ID));
        textTommorow = jsonFromUrl(new URL(WEATHER_TOMMOROW_APP_QUERY + this.city + WEATHER_APP_ID));

        tempToday = getSomeInfo(textToday, "$.main.temp");
        tempTommorow = getTommorowTemperature(textTommorow);
    }
    /**
     * Возвращает температуру на данный момент
     * @return Строка-температура
     */
    public String getTempToday() {
        return tempToday;
    }
    
    /**
     * Возвращает прогноз погоды на завтрашний день
     * @return Строка-температура завтра днём
     */
    public String getTempTommorow() {
        return tempTommorow;
    }

    private void setCity(String city) {
        this.city = (String) prop.get(city);
    }

    //получение данных по шаблону из json
    @Override
    public String getSomeInfo(String text, String param) {
        int temperature = (int) (Math.round(Double.valueOf(JsonPath.read(text, param).toString())));
        return Integer.toString(temperature);
    }

    //для получения погоды на завтра испоьзуется прогноз погоды с 3-х часовым шагом
    //погодой на завтра считается погода в 15-00
    //прогноз получается с сайта в формате json
    private String getTommorowTemperature(String text) {
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DATE, 1);
        Date tommorow = date.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String tomString = format.format(tommorow) + " 15:00:00";

        List<Object> list = JsonPath.parse(text).read("$..list[?(@.dt_txt=='" + tomString + "')].main.temp");
        int temperature = (int) (Math.round((double) list.get(0)));
        return Integer.toString(temperature);
    }
}
