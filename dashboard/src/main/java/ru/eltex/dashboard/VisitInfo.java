package ru.eltex.dashboard;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import static com.mongodb.client.model.Projections.*;
/**
 * Класс, отвечающий за счетчик посещений
 * 
 * @author Khodorchenko Daria
 *
 */
public class VisitInfo {
    //параметры подключения к MongoDB вынесены в файл конфигурации
    private final static String CONNECTION_PROP = "src/main/resources/mongoClient.properties";

    private MongoClient mongo;
    private MongoDatabase db;
    private MongoCollection collection;
    private int count = 0;
    private FileInputStream fis;
    private Properties prop = new Properties();
    
    /**
     * Задаются параметры подключения к Mongo,
     * осуществляется подключение, выбор базы данных и коллекции
     * @throws MongoException Может возникнуть при подключении к MongoDB
     * @throws IOException Может возникнуть при получении параметров из файла конфигурации
     */
    public VisitInfo() throws MongoException, IOException {
        fis = new FileInputStream(CONNECTION_PROP);
        prop.load(fis);//загрузка параметров

        //изменение настроек подключения
        //уменьшено время ожидания ответа
        MongoClientOptions.Builder optionsBulder = MongoClientOptions.builder();
        optionsBulder.connectTimeout(Integer.valueOf(prop.getProperty("timeout")));
        optionsBulder.serverSelectionTimeout(Integer.valueOf(prop.getProperty("timeout")));
        MongoClientOptions options = optionsBulder.build();

        mongo = new MongoClient(prop.getProperty("host"), options);
        db = mongo.getDatabase(prop.getProperty("dbname"));
        collection = db.getCollection(prop.getProperty("collection"));
    }
    
    /**
     * Добавляет одно посещение при вызове метода и возвращает общее количество посещений
     * 
     * @return Количество посещений страницы приложения
     */
    public int getCount() {
        // счетчик выполнен с использованием функции upsert БД
        collection.updateOne(new Document("page", "dashboard"), new Document("$inc", new Document("hits", 1)), new UpdateOptions().upsert(true));
        FindIterable hits = collection.find().projection(fields(include("hits"), excludeId()));
        ArrayList<Document> list = new ArrayList<>();
        hits.into(list);

        Document document = list.get(0);
        Object obj = document.get("hits");
        count = (int) obj;

        return count;
    }


}
