package ru.eltex.dashboard;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
/**
 * Класс, отвечающий за отображение погодных данных
 * 
 * @author Khodorchenko Daria
 *
 */
public class WeatherWidget extends CustomComponent {

    final static Logger logger = Logger.getLogger(WeatherWidget.class);
    //список городов вынесен в файл конфигурации
    private final static String CITY_NAMES = "src/main/resources/cityname.properties";

    private String city;
    private WeatherInfo weather;
    private FileInputStream fis;
    private Properties prop = new Properties();
    private List<String> cities = new ArrayList<>();

    /**
     * Задается отображение списка городов, погодных данных,
     * добавлятся кнопка "Обновить" и реакция на нажатие,
     * ведется обработка исключений.
     */
    public WeatherWidget() {

        Panel panel = new Panel("Погода");

        VerticalLayout panelContent = new VerticalLayout();
        panel.setContent(panelContent);
        panelContent.setSizeFull();

        VerticalLayout weatherLayout = new VerticalLayout();
        weatherLayout.setMargin(false);

        try {
            fis = new FileInputStream(CITY_NAMES);
            prop.load(fis);

            NativeSelect selectCity = new NativeSelect("Выберите город");
            // получение списка городов из файла с учетом кодировки
            for (String key : prop.stringPropertyNames()) {
                int i = 0;
                String cityName = new String(prop.getProperty(key).getBytes("ISO8859-1"));
                cities.add(cityName);
            }
            Collections.sort(cities);//сортировка городов в алфавитном порядке
            selectCity.setItems(cities);
            selectCity.setValue(cities.get(0));
            selectCity.setEmptySelectionAllowed(false);
            panelContent.addComponent(selectCity);

            city = getCityKey(String.valueOf(selectCity.getValue()));

            addWeatherInfo(weatherLayout, city);
            panelContent.addComponent(weatherLayout);

            selectCity.addValueChangeListener(event -> {
                city = getCityKey(String.valueOf(selectCity.getValue()));
                changeWeatherInfo(weatherLayout, city);
            });

        } catch (FileNotFoundException e) {
            logger.error("Файл настроек не обнаружен", e);
            Label error = new Label("Список городов недоступен");
            panelContent.addComponent(error);
        } catch (IOException e) {
            logger.error("Ошибка загрузки файла", e);
            Label error = new Label("Список городов недоступен");
            panelContent.addComponent(error);
        }

        Button refresh = new Button("Обновить");
        refresh.addClickListener(clickEvent -> {
            changeWeatherInfo(weatherLayout, city);
        });

        panelContent.addComponent(refresh);
        panelContent.setComponentAlignment(refresh, Alignment.BOTTOM_CENTER);

        setCompositionRoot(panel);
        addStyleName(ValoTheme.PANEL_BORDERLESS);
        panel.setHeight("100%");
    }

    //добавление погдных данных
    private void addWeatherInfo(Layout layout, String city) {
        try {
            weather = new WeatherInfo(city);

            Label today = new Label("Сейчас: " + weather.getTempToday()+"\u2103");
            Label tommorow = new Label("Завтра днём: " + weather.getTempTommorow()+"\u2103");

            layout.addComponents(today, tommorow);

        } catch (IOException e) {
            logger.error("Данные о погоде недоступны", e);
            Label error = new Label("Данные о погоде\nнедоступны");
            error.setContentMode(ContentMode.PREFORMATTED);
            layout.addComponent(error);
        }
    }

    private void changeWeatherInfo(Layout layout, String city) {
        layout.removeAllComponents();
        addWeatherInfo(layout, city);
    }

    //получение ключа по название города для запроса прогноза погоды
    private String getCityKey(String city) {

        for (String key : prop.stringPropertyNames()) {
            String cityName = null;
            try {
                cityName = new String(prop.getProperty(key).getBytes("ISO8859-1"));
            } catch (UnsupportedEncodingException e) {
                logger.error("Ошибка обработки кодировки", e);
            }
            if (city.equals(cityName))
                return key;
        }
        return null;
    }
}
