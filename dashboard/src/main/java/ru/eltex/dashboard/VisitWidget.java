package ru.eltex.dashboard;

import com.mongodb.MongoException;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.apache.log4j.Logger;

import java.io.IOException;
/**
 * Класс, отвечающий за отображение информации о посещениях
 * @author Khodorchenko Daria
 *
 */
public class VisitWidget extends CustomComponent {

    final static Logger logger = Logger.getLogger(VisitWidget.class);
    private VisitInfo visits;

    /**
     * Задает отображение информации о посещениях и обрабатывает исключения
     */
    public VisitWidget() {
        Panel panel = new Panel("Счетчик посещений");

        VerticalLayout panelContent = new VerticalLayout();
        panel.setContent(panelContent);
        panelContent.setSizeFull();

        try {
            visits = new VisitInfo();
            Label count = new Label("Количество\nпосещений:\n" + visits.getCount());
            count.setContentMode(ContentMode.PREFORMATTED);
            panelContent.addComponent(count);
            panelContent.setComponentAlignment(count, Alignment.MIDDLE_CENTER);

        } catch (MongoException e) {
            Label error = new Label("Ошибка\nподключения\nк БД");
            error.setContentMode(ContentMode.PREFORMATTED);
            panelContent.addComponent(error);
            panelContent.setComponentAlignment(error, Alignment.MIDDLE_CENTER);
            logger.error("Ошибка подключения к БД", e);
        } catch (IOException e) {
            Label error = new Label("Ошибка получения настроек");
            error.setContentMode(ContentMode.PREFORMATTED);
            logger.error("Ошибка получения настроек", e);
        }

        setCompositionRoot(panel);
        addStyleName(ValoTheme.PANEL_BORDERLESS);
        panel.setHeight("100%");
    }
}
