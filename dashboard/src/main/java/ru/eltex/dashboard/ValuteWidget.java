package ru.eltex.dashboard;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.apache.log4j.Logger;

import java.io.IOException;
/**
 * Класс, отвечающий за отображение информации о курсе валют для пользователя 
 * @author Khodorchenko Daria
 *
 */
public class ValuteWidget extends CustomComponent {

    final static Logger logger = Logger.getLogger(ValuteWidget.class);
    private ValuteInfo info;
    
    /**
     * Задается структура интерфейса, отрисовываются надписи и кнопка, задается действие по нажатию,
     * ведется обработка исключений
     */
    public ValuteWidget() {

        Panel panel = new Panel("Курс Валют");

        VerticalLayout panelContent = new VerticalLayout();
        panel.setContent(panelContent);
        panelContent.setSizeFull();

        VerticalLayout valuteRates = new VerticalLayout();
        valuteRates.setMargin(false);
        addValuteInfo(valuteRates);//вывод курса валют

        panelContent.addComponent(valuteRates);
        panelContent.setComponentAlignment(valuteRates, Alignment.MIDDLE_CENTER);

        Button refresh = new Button("Обновить");
        refresh.addClickListener(clickEvent -> {
            update(valuteRates);
        });
        panelContent.addComponent(refresh);
        panelContent.setComponentAlignment(refresh, Alignment.MIDDLE_CENTER);
        panelContent.setExpandRatio(valuteRates, 1);

        setCompositionRoot(panel);
        addStyleName(ValoTheme.PANEL_BORDERLESS);

        panel.setHeight("100%");
    }

    private void update(VerticalLayout vr) {
        vr.removeAllComponents();
        addValuteInfo(vr);
    }

    //добавление информации в layout
    private void addValuteInfo(VerticalLayout layout) {
        try {
            info = new ValuteInfo();

            Label usd = new Label("USD: " + info.getUsd()+" RUB");
            Label eur = new Label("EUR: " + info.getEur()+" RUB");
            layout.addComponent(usd);
            layout.setComponentAlignment(usd, Alignment.MIDDLE_CENTER);
            layout.addComponent(eur);
            layout.setComponentAlignment(eur, Alignment.MIDDLE_CENTER);

        } catch (IOException e) {
            logger.error("Курс валют не был получен с сайта", e);
            Label error = new Label("Курс валют\nнедоступен");
            error.setContentMode(ContentMode.PREFORMATTED);
            layout.addComponent(error);
        }
    }

}
