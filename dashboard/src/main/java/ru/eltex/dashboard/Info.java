package ru.eltex.dashboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 
 * @author Khodorchenko Daria
 * 
 * Абстрактный класс для получения и преобразования информации по URL в формате json
 *
 */

public abstract class Info {

    /**
     * Получение данных по URL и преобразование их в строку
     * 
     * @param url Адрес источника данных
     * @return Строка данных в формате json
     * @throws IOException Исключение может возникнуть при HTTP-подключении и получении потока данных из подключения
     */
    public String jsonFromUrl(URL url) throws IOException {
        String text = null;
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.connect();

        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = br.read()) != -1) {
            sb.append((char) cp);
        }
        text = sb.toString();

        return text;
    }
    
    /**
     * Получение данных из строки с форматом json по шаблону
     * @param string Строка данных
     * @param param Шаблон поиска
     * @return Строка-результат поиска
     */

    public abstract String getSomeInfo(String string, String param);


}
