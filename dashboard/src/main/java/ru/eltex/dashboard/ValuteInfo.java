package ru.eltex.dashboard;

import com.jayway.jsonpath.JsonPath;

import java.io.IOException;
import java.net.URL;

/**
 * 
 * @author Khodorchenko Daria
 *
 *Класс, получающий данные о курсе валют
 *
 */

public class ValuteInfo extends Info {

    private final static String cbrUrl = "https://www.cbr-xml-daily.ru/daily_json.js";

    private String usd;
    private String eur;
    private String text;

    /**
     * Получает данные от официального API
     * и извлекает из них нужные значения
     * @throws IOException Исключение может возникнуть при использовании методаjsonFromUrl, либо при создании URL
     */
    public ValuteInfo() throws IOException {
        text = jsonFromUrl(new URL(cbrUrl));
        usd = getSomeInfo(text, "USD");
        eur = getSomeInfo(text, "EUR");
    }
    
    /**
     * Возвращает значение курса доллара
     * @return Строка, содержащая значение курса доллара
     */
    public String getUsd() {
        return usd;
    }
    /**
     * Возвращает значение курса евро
     * @return Строка, содержащая значение курса евро
     */
    public String getEur() {
        return eur;
    }

   
    @Override
    public String getSomeInfo(String text, String valute) {
        return JsonPath.read(text, "$.Valute." + valute + ".Value").toString();
    }
}
