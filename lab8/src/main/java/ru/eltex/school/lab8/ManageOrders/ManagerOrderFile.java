package ru.eltex.school.lab8.ManageOrders;

import ru.eltex.school.lab8.Orders.Order;
import ru.eltex.school.lab8.Orders.Orders;

import java.io.*;
import java.util.Iterator;
import java.util.UUID;

public class ManagerOrderFile extends AManageOrder {


    public static final String BINFILE = "orders.dat";

    @Override
    public Order readById(UUID id) throws IOException {
        Order newOrd = null;
        Order temp;
        FileInputStream fis = new FileInputStream(BINFILE);
        ObjectInputStream oin = new ObjectInputStream(fis);
        try {

            while (fis.available() > 0) {
                temp = (Order) oin.readObject();
                if (temp.getUuid().equals(id)) {
                    newOrd = temp;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            oin.close();
        }

        return newOrd;

    }

    @Override
    public void saveById(Orders orders, UUID id) throws IOException {
        super.saveById(orders, id);
        if (readById(id) == null) {
            FileOutputStream fos = new FileOutputStream(BINFILE, true);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(order);
            oos.close();
        }
    }

    @Override
    public Orders readAll() throws IOException {
        Order newOrd;
        Orders newOrders = new Orders();
        FileInputStream fis = new FileInputStream(BINFILE);
        ObjectInputStream oin = new ObjectInputStream(fis);
        try {

            while (fis.available() > 0) {
                newOrd = (Order) oin.readObject();
                newOrders.add(newOrd);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            oin.close();
        }
        return newOrders;
    }

    @Override
    public void saveAll(Orders orders) throws IOException {
        ords = orders;

        FileOutputStream fos = new FileOutputStream(BINFILE);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        Iterator<Order> it = ords.getOrder().iterator();
        while (it.hasNext()) {
            Order ord = it.next();
            oos.writeObject(ord);
        }
        oos.close();
    }
}
