package ru.eltex.school.lab8.ClientServer;

import ru.eltex.school.lab8.Orders.Order;
import ru.eltex.school.lab8.Orders.Orders;
import ru.eltex.school.lab8.Orders.Status;

import java.util.Iterator;

public class OrdersCheck implements Runnable {

    public boolean frun = true;
    private Orders orders;
    private ClientIpMap ipMap;
    private UdpMessage udpserver = new UdpMessage();

    public OrdersCheck(Orders orders, ClientIpMap ipMap) {
        this.orders = orders;
        this.ipMap = ipMap;
    }

    @Override
    public void run() {
        while (frun) {
            try {
                synchronized (orders) {
                    Iterator<Order> it = orders.getOrder().iterator();
                    while (it.hasNext()) {
                        Order ord = it.next();
                        if (ord.getStat() == Status.WAIT) {
                            ord.setStat(Status.DONE);
                            Thread.sleep(1000);
                            String clientIp = ipMap.getIp(ord.getUuid());
                            int clientPort = ipMap.getPort(ord.getUuid());
                            udpserver.sendUdpMessage("Done", clientIp, clientPort);
                        }
                    }
                }
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
    }
}
