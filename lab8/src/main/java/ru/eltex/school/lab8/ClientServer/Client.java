package ru.eltex.school.lab8.ClientServer;

import ru.eltex.school.lab8.AChecks.OrderGenerator;
import ru.eltex.school.lab8.Orders.Order;
import ru.eltex.school.lab8.Orders.Orders;

import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;

public class Client {
    public static final int UDPPORT = 6666;
    public static int udpForTcp;
    public static int udpPort;

    public static void main(String[] args) {

        if (args.length > 0) {
            try {
                udpForTcp = Integer.parseInt(args[0]);
                udpPort = Integer.parseInt(args[1]);

                System.out.println("Client is running");

                DatagramSocket ds = new DatagramSocket(udpForTcp);
                DatagramPacket pack = new DatagramPacket(new byte[1024], 1024);
                ds.receive(pack);
                int tcpPort = Integer.parseInt(new String(pack.getData(), pack.getOffset(), pack.getLength()));
                InetAddress serverAddr = pack.getAddress();
                ds.close();

                while (true) {
                    Order order = new Client().genOrd();

                    Socket socket = new Socket(serverAddr, tcpPort);
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(order);
                    oos.writeObject(udpPort);

                    long startCheck = System.currentTimeMillis();

                    oos.close();
                    socket.close();

                    ds = new DatagramSocket(udpPort);
                    pack = new DatagramPacket(new byte[1024], 1024);
                    ds.receive(pack);
                    String status = new String(pack.getData(), pack.getOffset(), pack.getLength());
                    ds.close();

                    if (status.equals("Done")) {
                        long endCheck = System.currentTimeMillis() - startCheck;
                        System.out.println("------");
                        System.out.println("Время обработки заказа: " + endCheck + " ms");
                        order.show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("No UDP ports");
        }
    }

    public Order genOrd() {
        Orders<Order> orders = new Orders<>();
        OrderGenerator generator = new OrderGenerator(orders);
        Thread genOrd = new Thread(generator);
        genOrd.start();

        try {
            genOrd.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return orders.getOrder().get(0);
    }

}
