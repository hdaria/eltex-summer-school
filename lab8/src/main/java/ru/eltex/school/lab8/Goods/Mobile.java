package ru.eltex.school.lab8.Goods;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;

@Entity
public class Mobile extends Goods implements Serializable {

    transient Scanner scan = new Scanner(System.in);
    transient Random random = new Random();
    private String casesType[] = {"Classic", "Clamshell"};

    transient private int typeOfCase;
    private String phoneCase;

    public Mobile() {
        super();
        typeOfCase = random.nextInt(casesType.length);
        phoneCase = casesType[typeOfCase];
    }

    public int getTypeOfCase() {
        return typeOfCase;
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Тип корпуса телефона: " + this.phoneCase);
        System.out.println(" ");
    }

    @Override
    public void update() {
        super.update();
        System.out.println("Введите тип корпуса телефона (0 - Classic, 1 - Clamshell):");
        this.typeOfCase = scan.nextInt();
    }

    @Override
    public void delete() {
        super.delete();
        this.phoneCase = null;
    }
}
