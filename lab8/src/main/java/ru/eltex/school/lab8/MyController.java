package ru.eltex.school.lab8;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.eltex.school.lab8.AChecks.GoodsGenerator;
import ru.eltex.school.lab8.Repository.CartRepository;
import ru.eltex.school.lab8.Repository.OrderRepository;


@RestController
public class MyController {
    private final static Logger logger = Logger.getLogger(MyController.class);

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CartRepository cartRepository;

    @GetMapping("/")
    public Object allRequests(@RequestParam("command") String command,
                              @RequestParam(value = "order_id", required = false) String orderId,
                              @RequestParam(value = "card_id", required = false) String cardId) {
        if (command.equalsIgnoreCase("readAll"))
            return orderRepository.findAll();
        else if (command.equalsIgnoreCase("readById")) {
            Long id = Long.parseLong(orderId);
            return orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Заказ " + id + " не найден"));
        } else if (command.equalsIgnoreCase("delById")) {
            Long id = Long.parseLong(orderId);
            return orderRepository.findById(id).map(order -> {
                orderRepository.delete(order);
                return ResponseEntity.ok().build();
            }).orElseThrow(() -> new ResourceNotFoundException("Заказ " + id + " не найден"));
        } else if (command.equalsIgnoreCase("addToCard")) {
            Long id = Long.parseLong(cardId);
            return cartRepository.findById(id).map(shoppingCart -> {
                shoppingCart.add(new GoodsGenerator().generateGood());
                return cartRepository.save(shoppingCart);
            });
        } else throw new ResourceNotFoundException("Wrong command");
    }

}
