package ru.eltex.school.lab8.Orders;

public class DeleteOrderException extends Exception {

    private int errorCode;

    public DeleteOrderException(String message, int code) {
        super(message);
        errorCode = code;
    }

    public int getErrorCode() {
        return errorCode;
    }

}
