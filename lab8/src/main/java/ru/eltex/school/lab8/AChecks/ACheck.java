package ru.eltex.school.lab8.AChecks;

import ru.eltex.school.lab8.Orders.Order;
import ru.eltex.school.lab8.Orders.Orders;

public abstract class ACheck implements Runnable {

    public boolean frun = true;
    protected Orders<Order> orders;

    public synchronized void setOrders(Orders<Order> orders) {
        this.orders = orders;
    }

}
