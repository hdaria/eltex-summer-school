package ru.eltex.school.lab8.Orders;

import java.io.Serializable;

public enum Status implements Serializable {WAIT, DONE}
