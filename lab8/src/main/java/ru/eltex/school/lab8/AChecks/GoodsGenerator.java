package ru.eltex.school.lab8.AChecks;

import ru.eltex.school.lab8.Goods.Goods;
import ru.eltex.school.lab8.Goods.Mobile;
import ru.eltex.school.lab8.Goods.Smart;
import ru.eltex.school.lab8.Goods.Tablet;

public class GoodsGenerator {

    private Goods product;


    public Goods generateGood() {

        int choice = (int) (Math.random() * 3);
        switch (choice) {
            case 0:
                product = new Mobile();
                break;
            case 1:
                product = new Tablet();
                break;
            case 2:
                product = new Smart();
                break;
            default:
                product = new Mobile();
        }
        return product;
    }

}
