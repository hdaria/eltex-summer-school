package ru.eltex.school.lab8.Goods;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;

@Entity
public class Tablet extends Goods implements Serializable {

    transient Random random = new Random();
    transient Scanner scan = new Scanner(System.in);

    private String processor;
    private String resolution;

    transient private String VideoProc[] = {"Qualcomm Adreno", "NVIDIA GeForce Tegra", "PowerVR", "ARM Mail"};
    transient private int video;
    transient private String ScreenResol[] = {"480x320", "800x480", "960x640", "1280x800"};
    transient private int screen;

    public Tablet() {
        super();
        video = random.nextInt(VideoProc.length);
        processor = VideoProc[video];
        screen = random.nextInt(ScreenResol.length);
        resolution = ScreenResol[screen];
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Процессор: " + this.processor);
        System.out.println("Разешение экрана: " + this.resolution);
        System.out.println(" ");
    }

    @Override
    public void update() {
        super.update();
        System.out.println("Введите тип видеопроцессора [0-" + (VideoProc.length - 1) + "]:");
        this.video = scan.nextInt();
        System.out.println("Вседите разрешение экрана [0-" + (ScreenResol.length - 1) + "]:");
        this.screen = scan.nextInt();
    }

    @Override
    public void delete() {
        super.delete();
        this.resolution = null;
        this.processor = null;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }
}
