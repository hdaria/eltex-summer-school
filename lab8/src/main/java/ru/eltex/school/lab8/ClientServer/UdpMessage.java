package ru.eltex.school.lab8.ClientServer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpMessage {


    public UdpMessage() {
    }

    public void sendUdpMessage(String message, String host, int port) {
        try {
            byte[] data = message.getBytes();
            InetAddress addr = InetAddress.getByName(host);
            DatagramSocket ds = new DatagramSocket();
            DatagramPacket pack = new DatagramPacket(data, data.length, addr, port);
            ds.send(pack);
            ds.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
