package ru.eltex.school.lab8.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.eltex.school.lab8.Orders.Credentials;

@Repository
public interface UserRepository extends JpaRepository<Credentials, Long> {
}
