package ru.eltex.school.lab8.AChecks;

import ru.eltex.school.lab8.Goods.Goods;
import ru.eltex.school.lab8.Orders.Credentials;
import ru.eltex.school.lab8.Orders.Order;
import ru.eltex.school.lab8.Orders.Orders;
import ru.eltex.school.lab8.Orders.ShoppingCart;

public class OrderGenerator implements Runnable {

    public static final int MAX_GOODS = 5;
    public static final int MAX_ORDERS = 5;
    private Orders<Order> orders;

    public OrderGenerator(Orders ord) {
        orders = ord;

    }

    @Override
    public void run() {
        try {
            int countOrd = 2 + (int) (Math.random() * MAX_ORDERS);
            // int countOrd = 1;
            for (int i = 0; i < countOrd; i++) {
                Credentials user = new Credentials();
                ShoppingCart<Goods> cart = new ShoppingCart<>();
                int countGoods = 1 + (int) (Math.random() * MAX_GOODS);
                GoodsGenerator generator = new GoodsGenerator();
                for (int j = 0; j < countGoods; j++) {
                    Goods product = generator.generateGood();

                    cart.add(product);
                }

                synchronized (orders) {
                    orders.buy(cart, user);
                }

                orders.showOrders();

            }
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
