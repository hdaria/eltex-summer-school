package ru.eltex.school.lab8.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.eltex.school.lab8.Orders.ShoppingCart;

@Repository
public interface CartRepository extends JpaRepository<ShoppingCart, Long> {
}
