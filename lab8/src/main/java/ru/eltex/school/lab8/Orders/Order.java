package ru.eltex.school.lab8.Orders;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;
import ru.eltex.school.lab8.Goods.Goods;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Random;
import java.util.UUID;


@Entity
@Table(name = "orders")
public class Order implements Serializable {

    transient Random random = new Random();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //  @JsonProperty("cart")
    @OneToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "cart_id", nullable = false)
    private ShoppingCart cart;

    //@JsonProperty("user")
    @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Credentials user;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_time", nullable = false, updatable = false)
    @CreatedDate
    @NotNull
    private Date createTime;
    @Column(name = "wait_time")
    private int waitTime;
    @Column(name = "status")
    private Status stat;
    private UUID uuid;

    public Order() {
    }

    @JsonCreator
    public Order(@JsonProperty("cart") ShoppingCart cart1, @JsonProperty("user") Credentials user1) {
        uuid = UUID.randomUUID();
        cart = cart1;
        user = user1;
        createTime = new Date(System.currentTimeMillis());
        waitTime = 2000 + random.nextInt(8001);
        stat = Status.WAIT;
    }

    public void show() {
        System.out.println("Номер заказа: " + this.uuid);
        System.out.println("Состав заказа:");
        this.cart.showCart();
        System.out.println("Данные заказчика: ");
        this.user.show();
        System.out.println("Статус заказа: " + this.stat);
        System.out.println(" ");
    }

    public boolean addToCard(Goods item) {
        return this.cart.add(item);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public void setCart(ShoppingCart cart) {
        this.cart = cart;
    }

    public Credentials getUser() {
        return user;
    }

    public void setUser(Credentials user) {
        this.user = user;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(int waitTime) {
        this.waitTime = waitTime;
    }

    public Status getStat() {
        return stat;
    }

    public void setStat(Status stat) {
        this.stat = stat;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
