package ru.eltex.school.lab8.ClientServer;

import ru.eltex.school.lab8.Orders.Orders;

import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static final String TCPPORT = "6001";
    //  private static final String BROADCAST_ADDRESS = "255.255.255.255";
    private static Orders orders = new Orders();
    private static ClientIpMap ipMap = new ClientIpMap();
    private static String broadcastAddress;


    public static void main(String[] args) {

        if (args.length > 0) {

            broadcastAddress = args[0];

            try {
                System.out.println("Server is running");

                UdpTcpPort udpMessage = new UdpTcpPort(TCPPORT, broadcastAddress);
                Thread udpThread = new Thread(udpMessage);
                udpThread.start();

                ServerSocket ss = new ServerSocket(Integer.parseInt(TCPPORT));

                OrdersCheck check = new OrdersCheck(orders, ipMap);
                Thread checkThread = new Thread(check);
                checkThread.start();

                while (true) {
                    Socket s = ss.accept();
                    ServerConnectionProcessor p =
                            new ServerConnectionProcessor(s);
                    p.setParameters(orders, ipMap);
                    p.start();
                }

            } catch (Exception e) {
                System.err.println(e);
            }

        } else {
            System.out.println("Wrong brodcast address");
        }
    }
}
