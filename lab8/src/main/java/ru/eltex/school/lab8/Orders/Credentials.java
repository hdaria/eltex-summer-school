package ru.eltex.school.lab8.Orders;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Scanner;
import java.util.UUID;

@Entity
@Table(name = "users")
public class Credentials implements Serializable {

    transient Scanner scan = new Scanner(System.in);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private UUID uuid;
    private String name;
    private String surname;
    private String patronymic;
    private String email;

    public Credentials() {
        uuid = UUID.randomUUID();
        name = "Иван";
        surname = "Иванов";
        patronymic = "Иванович";
        email = "ivan@mailbox.com";
    }

    public void update() {
        System.out.println("Введите фамилию:");
        this.surname = scan.next();
        System.out.println("Введите имя:");
        this.name = scan.next();
        System.out.println("Введите отчество:");
        this.patronymic = scan.next();
        System.out.println("Введите e-mail:");
        this.email = scan.next();
    }


    public void show() {
        System.out.println("ID: " + this.uuid);
        System.out.println("Фамилия: " + this.surname);
        System.out.println("Имя: " + this.name);
        System.out.println("Отчество: " + this.patronymic);
        System.out.println("E-mail: " + this.email);
        System.out.println(" ");
    }

    public void delete() {
        this.name = null;
        this.surname = null;
        this.patronymic = null;
        this.email = null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
