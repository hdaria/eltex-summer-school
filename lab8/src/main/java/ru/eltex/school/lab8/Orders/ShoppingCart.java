package ru.eltex.school.lab8.Orders;

import ru.eltex.school.lab8.Goods.Goods;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;


@Entity
@Table(name = "carts")
public class ShoppingCart<T extends Goods> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(targetEntity = Goods.class, fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "cart_items",
            joinColumns = {@JoinColumn(name = "cart_id")},
            inverseJoinColumns = {@JoinColumn(name = "item_id")})
    private List<T> items = new LinkedList<>();


    public ShoppingCart() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public boolean add(T product) {
        return items.add(product);
    }

    public boolean delete(T product) {
        return items.remove(product);
    }

    public void showCart() {
        for (T prod : items) {
            prod.read();
        }
    }

}
