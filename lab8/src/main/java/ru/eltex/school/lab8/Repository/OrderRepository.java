package ru.eltex.school.lab8.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.eltex.school.lab8.Orders.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    Order findByCart_Id(Long cardId);

}
