package ru.eltex.school.lab8.Goods;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import ru.eltex.school.lab8.Orders.ShoppingCart;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@Entity
@Table(name = "items")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Goods implements ICrudAction, Serializable {

    public static final int MAX_PRICE = 1000;
    static private int goodsCount;
    transient Scanner scan = new Scanner(System.in);
    transient Random random = new Random();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private UUID uuid;
    private String type;

    transient private int name;
    transient private String Names[] = {"Мобильный телефон", "Смартфон", "Планшет"};

    private String firma;

    transient private int firm;
    transient private String Firms[] = {"Sony", "Samsung", "Panasonic", "Apple", "Xiaomi", "Siemens", "Toshiba", "Huawei"};


    private int model;
    private double price;
    private String system;

    transient private int os;
    transient private String Oses[] = {"None", "Android", "iOS", "Tizen", "WindowsMobile", "BlackBerry OS", "Sailfish OS", "Fire OS"};


    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "items")
    @JsonIgnore
    private Set<ShoppingCart> carts = new HashSet<>();


    Goods() {
        uuid = UUID.randomUUID();
        name = random.nextInt(Names.length);
        type = Names[name];
        firm = random.nextInt(Firms.length);
        firma = Firms[firm];
        model = 1000 + random.nextInt(9000);
        price = Math.random() * MAX_PRICE;
        os = random.nextInt(Oses.length);
        system = Oses[os];
        goodsCount++;
    }

    @Override
    public void read() {
        System.out.println("Название товара: " + this.type);
        System.out.println("Производитель товара: " + this.firma);
        System.out.println("Модель товара: " + this.model);
        System.out.println("Цена товара: " + this.price);
        System.out.println("Операционная система товара: " + this.system);
    }

    @Override
    public void update() {
        System.out.println("Введите название товара (0 - Мобильнй телефон, 1 - Смартфон, 2 - Планшет):");
        this.name = scan.nextInt();
        System.out.println("Введите производителя [0-" + (Firms.length - 1) + "]: ");
        this.firm = scan.nextInt();
        System.out.println("Введите модель:");
        this.model = scan.nextInt();
        System.out.println("Введите цену:");
        this.price = scan.nextDouble();
        System.out.println("Введите тип операционной системы [0-" + (Oses.length - 1) + "]:");
        this.os = scan.nextInt();
    }

    @Override
    public void delete() {
        this.type = null;
        this.firma = null;
        this.model = 0;
        this.system = null;
        this.price = 0;
        goodsCount--;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public Set<ShoppingCart> getCarts() {
        return carts;
    }

    public void setCarts(Set<ShoppingCart> carts) {
        this.carts = carts;
    }
}
