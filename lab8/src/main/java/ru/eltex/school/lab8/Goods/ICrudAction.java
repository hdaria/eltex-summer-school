package ru.eltex.school.lab8.Goods;

public interface ICrudAction {

    void read();

    void update();

    void delete();
}
