package ru.eltex.school.lab8.Goods;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;

@Entity
public class Smart extends Goods implements Serializable {

    transient Random random = new Random();
    transient Scanner scan = new Scanner(System.in);

    transient private String simTypes[] = {"SIM", "microSIM", "nanoSIM"};
    transient private int simType;

    private String typeSim;
    private int simNumber;

    public Smart() {
        super();
        simType = random.nextInt(simTypes.length);
        typeSim = simTypes[simType];
        simNumber = 1 + random.nextInt(2);
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Тип сим-карты: " + this.simType);
        System.out.println("Количество сим-карт: " + this.simNumber);
        System.out.println(" ");
    }

    @Override
    public void update() {
        super.update();
        System.out.println("Введите тип сим-карты (0 - SIM, 1 - microSIM, 2 - nanoSIM):");
        this.simType = scan.nextInt();
        System.out.println("Введите количество сим-карт:");
        this.simNumber = scan.nextInt();
    }

    @Override
    public void delete() {
        super.delete();
        this.simNumber = 0;
        this.typeSim = null;
    }

    public String getTypeSim() {
        return typeSim;
    }

    public void setTypeSim(String typeSim) {
        this.typeSim = typeSim;
    }

    public int getSimNumber() {
        return simNumber;
    }

    public void setSimNumber(int simNumber) {
        this.simNumber = simNumber;
    }
}
